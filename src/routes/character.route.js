const express = require("express");

const router = express.Router();
const passport = require("passport");

const CharacterController = require("../controllers/character.controller");

require("../middleware/passport")(passport);

/**
 * CRUD
 */
router.get("/", CharacterController.listAll);
router.get("/id/:id", CharacterController.findByPk);
router.post("/", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        CharacterController.create(err, user, req, res);
    })(req, res);
});


router.get("/name", CharacterController.findByName);

module.exports = router;

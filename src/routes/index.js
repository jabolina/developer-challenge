const express = require("express");

const router = express.Router();
const fs = require("fs");
const path = require("path");

router.get("/", (req, res) => {
    res.render("pages/index.ejs", {
        title: "Express",
    });
});

router.get("/views/:name", (req, res) => {
    let view;

    try {
        view = fs.readFileSync(path.join(__dirname, `../dist/views/pages/${req.params.name}`));
    } catch (e) {
        view = "";
    }

    res.header("Content-type", "text/html");
    return res.end(view);
});

module.exports = router;

const express = require("express");

const router = express.Router();
const passport = require("passport");

const UserController = require("../controllers/user.controller");

require("../middleware/passport")(passport);

/**
 * CRUD
 */
router.post("/", UserController.create);
router.get("/:id/", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        UserController.getOne(err, user, req, res);
    })(req, res);
});
router.get("/", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        UserController.getAll(err, user, req, res);
    })(req, res);
});
router.put("/", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        UserController.update(err, user, req, res);
    })(req, res);
});
router.delete("/:id", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        UserController.delete(err, user, req, res);
    })(req, res);
});
router.post("/login", UserController.login);


module.exports = router;

const express = require("express");

const router = express.Router();
const passport = require("passport");

const SeriesController = require("../controllers/series.controller");

require("../middleware/passport")(passport);

/**
 * CRUD
 */
router.get("/", SeriesController.listAll);
router.get("/id/:id", SeriesController.findByPk);
router.post("/", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        SeriesController.create(err, user, req, res);
    })(req, res);
});

router.get("/name", SeriesController.findByName);

module.exports = router;

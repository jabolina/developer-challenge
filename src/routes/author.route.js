const express = require("express");

const router = express.Router();
const passport = require("passport");

const AuthorController = require("../controllers/author.controller");

require("../middleware/passport")(passport);

/**
 * CRUD
 */
router.get("/", AuthorController.listAll);
router.get("/id/:id", AuthorController.findByPk);
router.post("/", (req, res) => {
    passport.authenticate("jwt", {
        session: false,
    }, (err, user) => {
        AuthorController.create(err, user, req, res);
    })(req, res);
});

router.get("/name", AuthorController.findByName);

module.exports = router;

const bodyParser = require("body-parser");
const favicon = require("serve-favicon");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const express = require("express");
const path = require("path");
const winston = require("winston");
const config = require("dotenv").load().parsed;

const indexRouter = require("./routes/index");
const userRouter = require("./routes/user.route");
const characterRouter = require("./routes/character.route");
const authorRouter = require("./routes/author.route");
const seriesRouter = require("./routes/series.route");

const app = express();

const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(info => `[${info.timestamp}] --- [${info.level}]: ${info.message}`),
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({
            filename: "application.log",
            level: "verbose",
        }),
    ],
});
const models = require("./models");

models.sequelize
    .authenticate()
    .then(() => {
        logger.info(`Connected into database ${config.DB_NAME}`);
        models.sequelize
            .sync()
            .then(() => {
                logger.info("Created tables!");
                models.User.create({
                    name: "marvelAdmin",
                    email: "admin@marvel.com",
                    password: "m4rv3l",
                    role: "admin",
                }).then((admin) => {
                    logger.info(`Created admin account ${JSON.stringify(admin, null, 2)}`);
                }).catch(() => {});
            })
            .catch((err) => {
                logger.error(`Error creating tables: ${err}`);
            });
    })
    .catch((err) => {
        logger.error(`Error connecting to database: ${err}`);
    });

app.set("views", path.join(__dirname, "dist/views"));
app.set("view engine", "ejs");

app.use(compression());
app.use(bodyParser.json({
    limit: "50mb",
}));
app.use(express.urlencoded({
    extended: false,
}));
app.use(cookieParser());
app.use(
    (req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
        res.header("Access-Control-Allow-Methods", "GET, POST");
        next();
    }, bodyParser.urlencoded({
        extended: true,
    }),
);
app.use(express.static(path.join(__dirname, "dist/views")));
app.use(favicon(path.join(__dirname, "dist/views/images/favicon.ico")));

app.use("/", indexRouter);
app.use("/user", userRouter);
app.use("/marvel", characterRouter);
app.use("/author", authorRouter);
app.use("/series", seriesRouter);

module.exports = {
    app,
    logger,
    config,
};

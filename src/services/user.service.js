const winston = require("winston");
const validator = require("validator");
const Sequelize = require("sequelize");
const {
    User,
} = require("../models");
const {
    to,
    Throws,
} = require("./util.service");

const { Op } = Sequelize;

const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(info => `[${info.timestamp}] --- [${info.level}]: ${info.message}`),
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({
            filename: "application.log",
            level: "verbose",
        }),
    ],
});

const findUniqueKey = (body) => {
    let uniqueKey = body.unique_key;

    if (!uniqueKey) {
        if (body.email) {
            uniqueKey = body.email;
        } else if (body.phone) {
            uniqueKey = body.phone;
        } else {
            uniqueKey = null;
        }
    }

    return uniqueKey;
};
module.exports.findUniqueKey = findUniqueKey;

const createUser = async (userInfo) => {
    let err; let
        user;
    const info = userInfo;

    const uniqueKey = findUniqueKey(info);

    if (!uniqueKey) {
        Throws(`Not found unique key in user info: ${JSON.stringify(info, null, 2)}`);
    }

    if (validator.isEmail(uniqueKey)) {
        info.email = uniqueKey;

        [err, user] = await to(User.create(info));
        if (err) {
            logger.error(`Error creating user: ${JSON.stringify(err, null, 2)}`);
            Throws(`User already exists with email [${uniqueKey}]`);
        }
    } else if (validator.isMobilePhone(uniqueKey, "any")) {
        info.method = uniqueKey;

        [err, user] = await to(User.create(info));
        if (err) {
            logger.error(`Error creating user: ${JSON.stringify(err, null, 2)}`);
            Throws(`User already exists with phone number [${uniqueKey}]`);
        }
    } else {
        Throws(`Not found valid unique key for user: ${JSON.stringify(info, null, 2)}`);
    }

    return user;
};
module.exports.createUser = createUser;

module.exports.listAll = async () => User.findAll({
    where: {
        role: {
            [Op.ne]: "admin",
        },
    },
});

module.exports.update = async (userInfo) => {
    const info = userInfo;

    const uniqueKey = findUniqueKey(info);

    if (!uniqueKey) {
        Throws(`Not found unique key in user info: ${JSON.stringify(info, null, 2)}`);
    }

    if (validator.isEmail(uniqueKey)) {
        info.email = uniqueKey;
    } else if (validator.isMobilePhone(uniqueKey, "any")) {
        info.method = uniqueKey;
    } else {
        Throws(`Not found valid unique key for user: ${JSON.stringify(info, null, 2)}`);
    }

    const [err, user] = await to(User.update({
        name: info.name,
        role: info.role,
    }, {
        where: {
            id: {
                [Op.eq]: info.id,
            },
        },
    }));

    if (err) {
        logger.error(`Error creating user: ${JSON.stringify(err, null, 2)}`);
        Throws(`Error creating user: ${err}`);
    }

    return user;
};

module.exports.deleteUser = async id => User.destroy({
    where: {
        id: {
            [Op.eq]: id,
        },
    },
});

module.exports.findOne = async id => User.findOne({
    where: {
        id: {
            [Op.eq]: id,
        },
    },
});

module.exports.authUser = async (userInfo) => {
    let err; let
        user;

    const uniqueKey = findUniqueKey(userInfo);

    if (!userInfo.password) {
        Throws("Not found password to login");
    }

    if (!uniqueKey) {
        Throws("Not found e-mail or phone number to login");
    }

    if (validator.isEmail(uniqueKey)) {
        [err, user] = await to(User.findOne({
            where: {
                email: uniqueKey,
            },
        }));

        if (err) {
            Throws(err.message);
        }
    } else if (validator.isMobilePhone(uniqueKey, "any")) {
        [err, user] = await to(User.findOne({
            where: {
                phone: uniqueKey,
            },
        }));

        if (err) {
            Throws(err.message);
        }
    } else {
        Throws("Email or phone number is not valid");
    }

    if (!user) {
        Throws("User not registered");
    }

    [err, user] = await to(user.comparePassword(userInfo.password));
    if (err) {
        Throws(err.message);
    }

    return user;
};

module.exports.roleValidation = (roles, user) => roles.indexOf(user.role) > -1;

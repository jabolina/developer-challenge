const Sequelize = require("sequelize");
const {
    Author,
    Comic,
    Series,
    Story,
    Event,
    Character,
} = require("../models");
const {
    buildRequest,
    callMarvel,
    to,
    Throws,
} = require("./util.service");

const { Op } = Sequelize;
const baseURL = "series";

module.exports.create = async (seriesInfo) => {
    const {
        authors,
        characters,
        stories,
        comics,
        events,
        ...body
    } = seriesInfo;

    const [err, serie] = await to(Series.create(body));

    if (err) {
        Throws(`Error creating series: ${JSON.stringify(err, null, 2)}`);
    }

    try {
        if (authors && authors.length > 0) {
            authors.forEach((author) => {
                Author.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: author.marvelId,
                        },
                    },
                    defaults: author,
                }).spread((a) => {
                    serie.setAuthors(a);
                });
            });
        }

        if (characters && characters.length > 0) {
            characters.forEach((character) => {
                Character.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: character.marvelId,
                        },
                    },
                    defaults: character,
                }).spread((c) => {
                    serie.setCharacters(c);
                });
            });
        }

        if (events && events.length > 0) {
            events.forEach((event) => {
                Event.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: event.marvelId,
                        },
                    },
                    defaults: event,
                }).spread((evt) => {
                    serie.setEvents(evt);
                });
            });
        }

        if (comics && comics.length > 0) {
            comics.forEach((comic) => {
                Comic.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: comic.marvelId,
                        },
                    },
                    defaults: comic,
                }).spread((c) => {
                    serie.addComics(c);
                });
            });
        }

        if (stories && stories.length > 0) {
            stories.forEach((story) => {
                Story.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: story.marvelId,
                        },
                    },
                    defaults: story,
                }).spread((s) => {
                    serie.setStories(s);
                });
            });
        }
    } catch (e) {
        Throws(`Error creating series associations: ${e}`);
    }

    return serie;
};

module.exports.listAllFromMarvel = async query => callMarvel(buildRequest(baseURL, query));

module.exports.findByNameDatabase = async (name) => {
    const [err, series] = await to(Series.findAll({
        where: {
            title: {
                [Op.like]: `${decodeURI(name)}%`,
            },
        },
        order: [
            ["title", "ASC"],
        ],
    }));

    if (err) {
        Throws(`Error searching series name like ${name} in database: ${JSON.stringify(err, null, 2)}`);
    }

    return series;
};

module.exports.findByNameMarvel = async nameQuery => callMarvel(buildRequest(baseURL, nameQuery));

module.exports.findByPkDatabase = async (id, useDbId) => {
    let err; let
        series;

    const include = [
        {
            model: Author,
            as: "authors",
            through: {
                attributes: [],
            },
        },
        {
            model: Character,
            as: "characters",
            through: {
                attributes: [],
            },
        },
        {
            model: Comic,
            as: "comics",
            through: {
                attributes: [],
            },
        },
        {
            model: Event,
            as: "events",
            through: {
                attributes: [],
            },
        },
        {
            model: Story,
            as: "stories",
            through: {
                attributes: [],
            },
        },
    ];

    if (useDbId) {
        [err, series] = await to(Series.findByPk(id, {
            include,
        }));
    } else {
        [err, series] = await to(Series.findAll({
            where: {
                marvelId: {
                    [Op.eq]: id,
                },
            },
            include,
        }));
    }

    if (err) {
        Throws(`Error searching by id in database: ${JSON.stringify(err, null, 2)}`);
    }

    return series;
};

module.exports.findByPkMarvel = async id => callMarvel(buildRequest(`${baseURL}/${id}`, ""));

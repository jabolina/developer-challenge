const Sequelize = require("sequelize");
const {
    Character,
    Comic,
    Series,
    Story,
    Event,
} = require("../models");
const {
    buildRequest,
    callMarvel,
    to,
    Throws,
    MD5,
} = require("./util.service");

const { Op } = Sequelize;
const baseURL = "characters";

module.exports.createCharacter = async (characterInfo) => {
    const info = characterInfo;

    info.randomId = MD5(characterInfo.modified);

    const {
        comics,
        series,
        events,
        stories,
        ...body
    } = info;

    const [err, character] = await to(Character.create(body));

    if (err) {
        Throws(`An error happened while saving character ${JSON.stringify(info, null, 2)}`);
    }

    try {
        if (comics && comics.length > 0) {
            comics.forEach((comic) => {
                Comic.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: comic.marvelId,
                        },
                    },
                    defaults: comic,
                }).spread((c) => {
                    character.setComics(c);
                });
            });
        }

        if (series && series.length > 0) {
            series.forEach((serie) => {
                Series.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: serie.marvelId,
                        },
                    },
                    defaults: serie,
                }).spead((s) => {
                    character.setSeries(s);
                });
            });
        }

        if (events && events.length > 0) {
            events.forEach((event) => {
                Event.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: event.marvelId,
                        },
                    },
                    defaults: event,
                }).spread((e) => {
                    character.setEvents(e);
                });
            });
        }

        if (stories && stories.length > 0) {
            stories.forEach((story) => {
                Story.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: story.marvelId,
                        },
                    },
                    defaults: story,
                }).spread((s) => {
                    character.setStories(s);
                });
            });
        }
    } catch (e) {
        Throws(`An error happened while associating values to character ${e}`);
    }

    return character;
};

module.exports.listAll = async () => {
    const [err, characters] = await to(Character.findAll({
        order: [
            ["name", "ASC"],
        ],
    }));

    if (err) {
        Throws(`Could not list characters with error: ${err}`);
    }

    return characters;
};

module.exports.listAllFromMarvel = async query => callMarvel(buildRequest(baseURL, query));

module.exports.findByNameDatabase = async (name) => {
    const [err, characters] = await to(Character.findAll({
        where: {
            name: {
                [Op.like]: `${decodeURI(name)}%`,
            },
        },
        order: [
            ["name", "ASC"],
        ],
    }));

    if (err) {
        Throws(`Error searching character name like ${name} in database: ${err}`);
    }

    return characters;
};

module.exports.findByNameMarvel = async nameQuery => callMarvel(buildRequest(baseURL, nameQuery));

module.exports.findByPkDatabase = async (id, useDbId) => {
    let err; let
        character;

    const include = [
        {
            model: Series,
            as: "series",
            through: {
                attributes: [],
            },
        },
        {
            model: Comic,
            as: "comics",
            through: {
                attributes: [],
            },
        },
        {
            model: Event,
            as: "events",
            through: {
                attributes: [],
            },
        },
        {
            model: Story,
            as: "stories",
            through: {
                attributes: [],
            },
        },
    ];

    if (useDbId) {
        [err, character] = await to(Character.findByPk(id, {
            include,
        }));
    } else {
        [err, character] = await to(Character.findAll({
            where: {
                marvelId: {
                    [Op.eq]: id,
                },
            },
            include,
        }));
    }

    if (err) {
        Throws(`Error searching character by id ${err}`);
    }

    return character;
};

module.exports.findByPkMarvel = async id => callMarvel(buildRequest(`${baseURL}/${id}`, ""));

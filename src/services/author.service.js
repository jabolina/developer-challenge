const Sequelize = require("sequelize");
const {
    Author,
    Comic,
    Series,
    Story,
    Event,
} = require("../models");
const {
    buildRequest,
    callMarvel,
    to,
    Throws,
} = require("./util.service");

const { Op } = Sequelize;

const baseURL = "creators";

module.exports.listAllFromMarvel = async query => callMarvel(buildRequest(baseURL, query));

module.exports.create = async (authorInfo) => {
    const {
        events,
        comics,
        series,
        stories,
        ...body
    } = authorInfo;
    const [err, author] = await to(Author.create(body));

    if (err) {
        Throws(`Error creating author: ${JSON.stringify(err, null, 2)}`);
    }

    try {
        if (events && events.length > 0) {
            events.forEach((event) => {
                Event.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: event.marvelId,
                        },
                    },
                    defaults: event,
                }).spread((evt) => {
                    author.setEvents(evt);
                });
            });
        }

        if (comics && comics.length > 0) {
            comics.forEach((comic) => {
                Comic.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: comic.marvelId,
                        },
                    },
                    defaults: comic,
                }).spread((c) => {
                    author.addComics(c);
                });
            });
        }

        if (series && series.length > 0) {
            series.forEach((serie) => {
                Series.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: serie.marvelId,
                        },
                    },
                    defaults: serie,
                }).spread((s) => {
                    author.setSeries(s);
                });
            });
        }

        if (stories && stories.length > 0) {
            stories.forEach((story) => {
                Story.findOrCreate({
                    where: {
                        marvelId: {
                            [Op.eq]: story.marvelId,
                        },
                    },
                    defaults: story,
                }).spread((s) => {
                    author.setStories(s);
                });
            });
        }
    } catch (e) {
        Throws(`Error associating values to author: ${e}`);
    }

    return author;
};

module.exports.findByNameDatabase = async (name) => {
    const [err, authors] = await to(Author.findAll({
        where: {
            name: {
                [Op.like]: `${decodeURI(name)}%`,
            },
        },
        order: [
            ["name", "ASC"],
        ],
    }));

    if (err) {
        Throws(`error searching author name like ${name} in database: ${err}`);
    }

    return authors;
};

module.exports.findByNameMarvel = async nameQuery => callMarvel(buildRequest(baseURL, nameQuery));

module.exports.findByPkDatabase = async (id, useDbId) => {
    let err; let
        author;

    const include = [
        {
            model: Series,
            as: "series",
            through: {
                attributes: [],
            },
        },
        {
            model: Comic,
            as: "comics",
            through: {
                attributes: [],
            },
        },
        {
            model: Event,
            as: "events",
            through: {
                attributes: [],
            },
        },
        {
            model: Story,
            as: "stories",
            through: {
                attributes: [],
            },
        },
    ];

    if (useDbId) {
        [err, author] = await to(Author.findByPk(id, {
            include,
        }));
    } else {
        [err, author] = await to(Author.findAll({
            where: {
                marvelId: {
                    [Op.eq]: id,
                },
            },
            include,
        }));
    }

    if (err) {
        Throws(`Error searching authors by id ${err}`);
    }

    return author;
};

module.exports.findByPkMarvel = async id => callMarvel(buildRequest(`${baseURL}/${id}`, ""));

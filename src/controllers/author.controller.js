const url = require("url");
const winston = require("winston");
const authorService = require("../services/author.service");
const { roleValidation } = require("../services/user.service");
const {
    to,
    ResWebErr,
    ResWebSuc,
    contains,
} = require("../services/util.service");

const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(info => `[${info.timestamp}] --- [${info.level}]: ${info.message}`),
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({
            filename: "character.controller.log",
            level: "verbose",
        }),
    ],
});

const marvelToAuthor = (marvelObject) => {
    const thumbnail = `${marvelObject.thumbnail.path}.${marvelObject.thumbnail.extension}`;

    return {
        marvelId: marvelObject.id,
        name: marvelObject.fullName,
        thumbnail,
        stories: marvelObject.stories.items.map(story => ({
            marvelId: story.resourceURI.split("/")[story.resourceURI.split("/").length - 1],
            name: story.name,
        })),
        series: marvelObject.series.items.map(serie => ({
            marvelId: serie.resourceURI.split("/")[serie.resourceURI.split("/").length - 1],
            title: serie.name,
        })),
        events: marvelObject.events.items.map(event => ({
            marvelId: event.resourceURI.split("/")[event.resourceURI.split("/").length - 1],
            name: event.name,
        })),
        comics: marvelObject.comics.items.map(comic => ({
            marvelId: comic.resourceURI.split("/")[comic.resourceURI.split("/").length - 1],
            name: comic.name,
        })),
        type: "author",
    };
};

const createDatabase = async (authorInfo) => {
    logger.info(`Creating author with: ${JSON.stringify(authorInfo, null, 2)}`);
    return to(authorService.create(authorInfo));
};

module.exports.create = async (err, user, req, res) => {
    if (err || !user) {
        return ResWebErr(res, "Não foi possível conferir a autenticação", 422);
    }

    if (!roleValidation(["editor", "admin"], user)) {
        return ResWebErr(res, "Usuário nao autorizado", 401);
    }

    const { body } = req;
    body.createdBy = user.email;

    const [dbErr, author] = await createDatabase(body);

    if (dbErr) {
        return ResWebErr(res, dbErr, 422);
    }

    return ResWebSuc(res, {
        message: "Successfully create new author",
        author: author.toWeb(),
    }, 200);
};

module.exports.listAll = async (req, res) => {
    logger.info("Receive list authors from Marvel");

    const parsed = url.parse(req.url);
    const response = [];

    const [err, authors] = await to(authorService.listAllFromMarvel(parsed.query));

    if (err) {
        logger.error(`Error listing authors from Marvel: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 422);
    }

    authors.forEach((author) => {
        response.push(marvelToAuthor(author));
    });

    return ResWebSuc(res, {
        message: "Success listing authors",
        authors: response.splice(0, 12),
    }, 200);
};

module.exports.findByName = async (req, res) => {
    logger.info("Searching for author by name");
    const parsed = url.parse(req.url);

    const response = [];
    const marvelIds = [];

    const [err, authors] = await to(authorService.findByNameDatabase(parsed.query.split("=")[1]));

    if (err) {
        logger.error(`Error listing author by name in database: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 422);
    }

    authors.forEach((author) => {
        const a = author.toWeb();
        a.type = "author";
        marvelIds.push(a.marvelId);
        response.push(a);
    });

    const [marvelErr, marvelAuthors] = await to(authorService.findByNameMarvel(parsed.query));

    if (marvelErr) {
        logger.error(`Error listing author by name from Marvel: ${JSON.stringify(marvelErr, null, 2)}`);
        return ResWebErr(res, marvelErr, 422);
    }

    marvelAuthors
        .filter(author => !contains(author.id, marvelIds))
        .map((author) => {
            const a = marvelToAuthor(author);
            a.type = "author";
            response.push(a);
            a.createdBy = "Marvel search";
            createDatabase(a);

            return a;
        });

    return ResWebSuc(res, {
        message: "Success finding by authors by name",
        authors: response,
    }, 200);
};

module.exports.findByPk = async (req, res) => {
    logger.info("Searching authors by Id");
    const { id } = req.params;
    const parsed = url.parse(req.url);
    let response;

    const [err, authors] = await to(authorService.findByPkDatabase(
        id, parsed.query.split("=")[1] === "true",
    ));

    if (err) {
        logger.error(`Error finding author by id: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 500);
    }

    response = authors.length && authors.length > 0
        ? authors[0]
        : authors;

    if (!response || response.length === 0) {
        const [marvelErr, marvelAuthors] = await to(authorService.findByPkMarvel(id));

        if (marvelErr) {
            logger.error(`Error finding by Id on Marvel: ${JSON.stringify(marvelErr, null, 2)}`);
            return ResWebErr(res, marvelErr, 500);
        }

        response = (marvelAuthors.length > 0)
            ? marvelToAuthor(marvelAuthors[0])
            : {};
    }

    return ResWebSuc(res, {
        message: `Success finding author with id ${id}`,
        author: response,
    });
};

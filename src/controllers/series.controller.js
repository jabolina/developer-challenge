const url = require("url");
const winston = require("winston");
const seriesService = require("../services/series.service");
const { roleValidation } = require("../services/user.service");
const {
    to,
    ResWebErr,
    ResWebSuc,
    contains,
    MD5,
} = require("../services/util.service");

const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(info => `[${info.timestamp}] --- [${info.level}]: ${info.message}`),
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({
            filename: "character.controller.log",
            level: "verbose",
        }),
    ],
});

const marvelToSeries = (marvelObject) => {
    const thumbnail = `${marvelObject.thumbnail.path}.${marvelObject.thumbnail.extension}`;

    return {
        marvelId: marvelObject.id,
        title: marvelObject.title,
        description: marvelObject.description,
        startYear: marvelObject.startYear,
        endYear: marvelObject.endYear,
        thumbnail,
        authors: marvelObject.creators.items.map(author => ({
            marvelId: author.resourceURI.split("/")[author.resourceURI.split("/").length - 1],
            name: author.name,
        })),
        characters: marvelObject.characters.items.map(character => ({
            marvelId: character.resourceURI.split("/")[character.resourceURI.split("/").length - 1],
            name: character.name,
            randomId: MD5(Date.now()),
        })),
        stories: marvelObject.stories.items.map(story => ({
            marvelId: story.resourceURI.split("/")[story.resourceURI.split("/").length - 1],
            name: story.name,
        })),
        comics: marvelObject.comics.items.map(comic => ({
            marvelId: comic.resourceURI.split("/")[comic.resourceURI.split("/").length - 1],
            name: comic.name,
        })),
        events: marvelObject.events.items.map(event => ({
            marvelId: event.resourceURI.split("/")[event.resourceURI.split("/").length - 1],
            name: event.name,
        })),
        type: "series",
    };
};

const createDatabase = async (body) => {
    logger.info(`Creating series with: ${JSON.stringify(body, null, 2)}`);

    return to(seriesService.create(body));
};

module.exports.create = async (err, user, req, res) => {
    if (err || !user) {
        return ResWebErr(res, "Não foi possível conferir a autenticação", 422);
    }

    if (!roleValidation(["editor", "admin"], user)) {
        return ResWebErr(res, "Usuário nao autorizado", 401);
    }

    const { body } = req;
    body.createdBy = user.email;

    const [dbErr, series] = await createDatabase(body);

    if (dbErr) {
        return ResWebErr(res, dbErr, 422);
    }

    return ResWebSuc(res, {
        message: "Success creating new serie",
        series,
    }, 200);
};

module.exports.listAll = async (req, res) => {
    logger.info("Receive list series from Marvel");

    const parsed = url.parse(req.url);
    const response = [];

    const [err, series] = await to(seriesService.listAllFromMarvel(parsed.query));

    if (err) {
        logger.error(`Error listing authors from Marvel: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 422);
    }

    series.forEach((serie) => {
        response.push(marvelToSeries(serie));
    });

    return ResWebSuc(res, {
        message: "Success listing series",
        series: response,
    }, 200);
};

module.exports.findByName = async (req, res) => {
    logger.info("Searching series by name");
    const parsed = url.parse(req.url);

    const response = [];
    const marvelIds = [];

    const [err, series] = await to(seriesService.findByNameDatabase(parsed.query.split("=")[1]));

    if (err) {
        logger.error(`Error listing series by name from database: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 422);
    }

    series.forEach((serie) => {
        const s = serie.toWeb();
        s.type = "series";
        marvelIds.push(s.marvelId);
        response.push(s);
    });

    const [marvelErr, marvelSeries] = await to(seriesService.findByNameMarvel(parsed.query));

    if (marvelErr) {
        logger.error(`Error listing series by name from Marvel: ${JSON.stringify(marvelErr, null, 2)}`);
        return ResWebErr(res, marvelErr, 422);
    }

    marvelSeries
        .filter(serie => !contains(serie.id, marvelIds))
        .map((serie) => {
            const s = marvelToSeries(serie);
            s.type = "series";
            response.push(s);
            s.createdBy = "Marvel search";
            createDatabase(s);

            return s;
        });

    return ResWebSuc(res, {
        message: "Success finding series by name",
        series: response,
    }, 200);
};

module.exports.findByPk = async (req, res) => {
    logger.info("Searching series by Id");
    const { id } = req.params;
    const parsed = url.parse(req.url);
    let response;

    const [err, series] = await to(seriesService.findByPkDatabase(
        id, parsed.query.split("=")[1] === "true",
    ));

    if (err) {
        logger.error(`Error finding series by Pk: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 500);
    }

    response = series.length && series.length > 0
        ? series[0]
        : series;

    if (!response || response.length === 0) {
        const [marvelErr, marvelSeries] = await to(seriesService.findByPkMarvel(id));

        if (marvelErr) {
            logger.error(`Error finding series in Marvel by Id: ${JSON.stringify(marvelErr, null, 2)}`);
            return ResWebErr(res, marvelErr, 500);
        }

        response = (marvelSeries.length > 0)
            ? marvelToSeries(marvelSeries[0])
            : {};
    }

    return ResWebSuc(res, {
        message: `Success finding series with id ${id}`,
        series: response,
    });
};

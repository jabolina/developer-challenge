const authService = require("../services/user.service");
const {
    to,
    ResWebErr,
    ResWebSuc,
} = require("../services/util.service");

module.exports.create = async (req, res) => {
    const { body } = req;

    if (!body.unique_key && !body.email && !body.phone) {
        return ResWebErr(res, `Neither email nor phone found in user ${JSON.stringify(body, null, 2)}`);
    } if (!body.password) {
        return ResWebErr(res, "Password not found in user");
    }

    body.role = body.role || "regular";
    const [err, user] = await to(authService.createUser(body));

    if (err) {
        return ResWebErr(res, err, 422);
    }

    return ResWebSuc(
        res, {
            message: "Successfully created new user.",
            user: user.toWeb(),
            token: user.getJWT(),
        }, 200,
    );
};

module.exports.getOne = async (authErr, authUser, req, res) => {
    const { id } = req.params;

    if (authErr || !authUser) {
        return ResWebErr(res, "Erro durante autenticação", 500);
    }

    if (!authService.roleValidation(["admin"], authUser)) {
        return ResWebErr(res, "Usuario não autorizado", 401);
    }

    const [err, user] = await to(authService.findOne(id));

    if (err) {
        return ResWebErr(res, "Error finding user to delete", 500);
    }

    return ResWebSuc(res, {
        message: `Sucess finding user with id ${id}`,
        user,
    });
};

module.exports.getAll = async (authErr, authUser, req, res) => {
    if (authErr || !authUser) {
        return ResWebErr(res, "Erro durante autenticação", 500);
    }

    if (!authService.roleValidation(["admin"], authUser)) {
        return ResWebErr(res, "Usuario não autorizado", 401);
    }

    const [err, users] = await to(authService.listAll());

    if (err) {
        return ResWebErr(res, `Error while listing users ${JSON.stringify(err, null, 2)}`, 500);
    }

    return ResWebSuc(
        res, {
            users: users.map(user => user.toWeb()),
        },
    );
};

module.exports.update = async (authErr, userModel, req, res) => {
    const data = req.body;

    if (authErr || !userModel) {
        return ResWebErr(res, "Erro durante autenticação", 500);
    }

    if (!authService.roleValidation(["admin"], userModel)) {
        return ResWebErr(res, "Usuario não autorizado", 401);
    }

    const [err, user] = await to(authService.update(data));
    if (err) {
        return ResWebErr(res, err);
    }

    return ResWebSuc(res, {
        message: `Updated user [${user.email}]`,
    });
};

module.exports.delete = async (authErr, authUser, req, res) => {
    const { id } = req.params;

    if (authErr || !authUser) {
        return ResWebErr(res, "Erro durante autenticação", 500);
    }

    if (!authService.roleValidation(["admin"], authUser)) {
        return ResWebErr(res, "Usuario não autorizado", 401);
    }

    const [err] = await to(authService.deleteUser(id));
    if (err) {
        return ResWebErr(res, err);
    }

    return ResWebSuc(res, {
        message: "User deleted.",
    }, 200);
};

module.exports.login = async (req, res) => {
    const [err, user] = await to(authService.authUser(req.body));
    if (err) {
        return ResWebErr(res, err, 422);
    }

    return ResWebSuc(
        res, {
            token: user.getJWT(),
            user: user.toWeb(),
        }, 200,
    );
};

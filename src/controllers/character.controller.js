const winston = require("winston");
const url = require("url");
const characterService = require("../services/character.service");
const { roleValidation } = require("../services/user.service");
const {
    to,
    ResWebErr,
    ResWebSuc,
    contains,
} = require("../services/util.service");

const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(info => `[${info.timestamp}] --- [${info.level}]: ${info.message}`),
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({
            filename: "character.controller.log",
            level: "verbose",
        }),
    ],
});

const createDatabase = async (body) => {
    logger.info(`Creating new character with: ${JSON.stringify(body, null, 2)}`);
    return to(characterService.createCharacter(body));
};

module.exports.create = async (err, user, req, res) => {
    if (err || !user) {
        return ResWebErr(res, "Não foi possível conferir a autenticação", 422);
    }

    if (!roleValidation(["editor", "admin"], user)) {
        return ResWebErr(res, "Usuário nao autorizado", 401);
    }

    const { body } = req;
    body.createdBy = user.email;

    const [dbErr, character] = await createDatabase(body);

    if (dbErr) {
        return ResWebErr(res, dbErr, 422);
    }

    return ResWebSuc(
        res, {
            message: "Successfully created new character.",
            user: character.toWeb(),
        }, 200,
    );
};

const marvelToCharacter = (marvelObject) => {
    const thumbnail = `${marvelObject.thumbnail.path}.${marvelObject.thumbnail.extension}`;

    return {
        marvelId: marvelObject.id,
        name: marvelObject.name,
        description: marvelObject.description,
        modified: marvelObject.modified,
        thumbnail,
        stories: marvelObject.stories.items.map(story => ({
            marvelId: story.resourceURI.split("/")[story.resourceURI.split("/").length - 1],
            name: story.name,
        })),
        series: marvelObject.series.items.map(serie => ({
            marvelId: serie.resourceURI.split("/")[serie.resourceURI.split("/").length - 1],
            title: serie.name,
        })),
        events: marvelObject.events.items.map(event => ({
            marvelId: event.resourceURI.split("/")[event.resourceURI.split("/").length - 1],
            name: event.name,
        })),
        comics: marvelObject.comics.items.map(comic => ({
            marvelId: comic.resourceURI.split("/")[comic.resourceURI.split("/").length - 1],
            name: comic.name,
        })),
        type: "character",
    };
};

module.exports.findByName = async (req, res) => {
    logger.info("Searching for character by name");
    const parsed = url.parse(req.url);

    const response = [];
    const marvelIds = [];

    const [err, characters] = await to(characterService.findByNameDatabase(parsed.query.split("=")[1]));
    if (err) {
        logger.error(`Error listing all characters ${err}`);
        return ResWebErr(res, err, 422);
    }

    characters
        .forEach((character) => {
            const char = character.toWeb();
            char.type = "character";
            marvelIds.push(char.marvelId);
            response.push(char);
        });

    const [marvelErr, marvelCharacters] = await to(characterService.findByNameMarvel(parsed.query));

    if (marvelErr) {
        logger.error(`Error listing all characters from Marvel: ${err}`);
        return ResWebErr(res, err, 422);
    }

    marvelCharacters
        .filter(character => !contains(character.id, marvelIds))
        .map((character) => {
            const char = marvelToCharacter(character);
            char.type = "character";
            response.push(char);
            char.createdBy = "Marvel search";
            createDatabase(char);

            return char;
        });

    return ResWebSuc(
        res, {
            message: "Success listing all characters",
            characters: response,
        }, 200,
    );
};

module.exports.listAll = async (req, res) => {
    logger.info("Receive list all characters from Marvel");

    const parsed = url.parse(req.url);
    const response = [];

    const [err, characters] = await to(characterService.listAllFromMarvel(parsed.query));

    if (err) {
        logger.error(`Error listing all characters from Marvel: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 422);
    }

    characters.forEach((character) => {
        response.push(marvelToCharacter(character));
    });

    return ResWebSuc(
        res, {
            message: "Success listing all characters",
            characters: response.splice(0, 12),
        }, 200,
    );
};

module.exports.findByPk = async (req, res) => {
    logger.info("Searching characters by Id");
    const { id } = req.params;
    const parsed = url.parse(req.url);
    let response;

    const [err, characters] = await to(characterService.findByPkDatabase(
        id, parsed.query.split("=")[1] === "true",
    ));

    if (err) {
        logger.error(`Error finding characters by id: ${JSON.stringify(err, null, 2)}`);
        return ResWebErr(res, err, 500);
    }

    response = characters.length && characters.length > 0
        ? characters[0]
        : characters;

    if (!response || response.length === 0) {
        const [marvelErr, marvelCharacters] = await to(characterService.findByPkMarvel(id));

        if (marvelErr) {
            logger.error(`Error finding by Id on Marvel: ${JSON.stringify(marvelErr, null, 2)}`);
            return ResWebErr(res, marvelErr, 500);
        }

        response = (marvelCharacters.length > 0)
            ? marvelToCharacter(marvelCharacters[0])
            : {};
    }

    return ResWebSuc(res, {
        message: `Success finding character with id ${id}`,
        character: response,
    });
};

/* eslint no-nested-ternary: 0 */

const config = require("dotenv").load().parsed;
const {
    ExtractJwt,
    Strategy,
} = require("passport-jwt");
const {
    User,
} = require("../models");
const {
    to,
} = require("../services/util.service");

module.exports = (passport) => {
    const opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.JWT_ENCRYPTION,
    };

    passport.use(new Strategy(opts, async (jwtPayload, done) => {
        const [err, user] = await to(User.findByPk(jwtPayload.user_id));

        return (err || !jwtPayload)
            ? done(true, false)
            : (user)
                ? done(false, user)
                : done(true, false);
    }));
};

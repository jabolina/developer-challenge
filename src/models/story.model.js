module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("Story", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        marvelId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        name: {
            type: DataTypes.STRING(128),
            unique: true,
        },
    });

    Model.associate = function associate(models) {
        Model.belongsToMany(models.Character, {
            through: "CharacterStories",
            as: "characters",
            foreignKey: "storyId",
        });

        Model.belongsToMany(models.Author, {
            through: "AuthorStories",
            as: "authors",
            foreignKey: "storyId",
        });

        Model.belongsToMany(models.Series, {
            through: "SeriesStories",
            as: "series",
            foreignKey: "storyId",
        });
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

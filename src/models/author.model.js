module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("Author", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        marvelId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        name: {
            type: DataTypes.STRING(128),
        },
        modified: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        thumbnail: {
            type: DataTypes.TEXT("long"),
            allowNull: true,
        },
        createdBy: {
            type: DataTypes.STRING,
        },
        updatedBy: {
            type: DataTypes.STRING,
        },
    });

    Model.associate = function associate(models) {
        Model.belongsToMany(models.Comic, {
            through: "AuthorComics",
            as: "comics",
            foreignKey: "authorId",
        });

        Model.belongsToMany(models.Series, {
            through: "AuthorSeries",
            as: "series",
            foreignKey: "authorId",
        });

        Model.belongsToMany(models.Story, {
            through: "AuthorStories",
            as: "stories",
            foreignKey: "authorId",
        });

        Model.belongsToMany(models.Event, {
            through: "AuthorEvents",
            as: "events",
            foreignKey: "authorId",
        });
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

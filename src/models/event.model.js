module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("Event", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        marvelId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        name: {
            type: DataTypes.STRING(128),
            unique: true,
        },
    });

    Model.associate = function associate(models) {
        Model.belongsToMany(models.Character, {
            through: "CharacterEvents",
            as: "characters",
            foreignKey: "eventId",
        });

        Model.belongsToMany(models.Author, {
            through: "AuthorEvents",
            as: "authors",
            foreignKey: "eventId",
        });

        Model.belongsToMany(models.Series, {
            through: "SeriesEvents",
            as: "series",
            foreignKey: "eventId",
        });
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("Character", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        marvelId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        randomId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        name: {
            type: DataTypes.STRING(128),
            unique: true,
        },
        description: {
            type: DataTypes.TEXT("long"),
            allowNull: true,
        },
        modified: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        thumbnail: {
            type: DataTypes.TEXT("long"),
            allowNull: true,
        },
        createdBy: {
            type: DataTypes.STRING,
        },
        updatedBy: {
            type: DataTypes.STRING,
        },
    });

    Model.associate = function associate(models) {
        Model.belongsToMany(models.Comic, {
            through: "CharacterComics",
            as: "comics",
            foreignKey: "characterId",
        });

        Model.belongsToMany(models.Series, {
            through: "CharacterSeries",
            as: "series",
            foreignKey: "characterId",
        });

        Model.belongsToMany(models.Story, {
            through: "CharacterStories",
            as: "stories",
            foreignKey: "characterId",
        });

        Model.belongsToMany(models.Event, {
            through: "CharacterEvents",
            as: "events",
            foreignKey: "characterId",
        });
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

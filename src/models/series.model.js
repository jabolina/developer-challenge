module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("Series", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        marvelId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        title: {
            type: DataTypes.STRING(128),
            unique: true,
        },
        thumbnail: {
            type: DataTypes.TEXT("long"),
            allowNull: true,
        },
        startYear: {
            type: DataTypes.INTEGER(20),
            allowNull: true,
        },
        endYear: {
            type: DataTypes.INTEGER(20),
            allowNull: true,
        },
        rating: {
            type: DataTypes.STRING(20),
            allowNull: true,
        },
        createdBy: {
            type: DataTypes.STRING,
        },
        updatedBy: {
            type: DataTypes.STRING,
        },
    });

    Model.associate = function associate(models) {
        Model.belongsToMany(models.Character, {
            through: "CharacterSeries",
            as: "characters",
            foreignKey: "seriesId",
        });

        Model.belongsToMany(models.Author, {
            through: "AuthorSeries",
            as: "authors",
            foreignKey: "seriesId",
        });

        Model.belongsToMany(models.Story, {
            through: "SeriesStories",
            as: "stories",
            foreignKey: "seriesId",
        });

        Model.belongsToMany(models.Comic, {
            through: "SeriesComics",
            as: "comics",
            foreignKey: "seriesId",
        });

        Model.belongsToMany(models.Event, {
            through: "SeriesEvents",
            as: "events",
            foreignKey: "seriesId",
        });
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

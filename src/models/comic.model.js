module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("Comic", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        marvelId: {
            type: DataTypes.INTEGER,
            unique: true,
        },
        name: {
            type: DataTypes.STRING(128),
            unique: true,
        },
    });

    Model.associate = function associate(models) {
        Model.belongsToMany(models.Character, {
            through: "CharacterComics",
            as: "characters",
            foreignKey: "comicId",
        });

        Model.belongsToMany(models.Author, {
            through: "AuthorComics",
            as: "authors",
            foreignKey: "comicId",
        });

        Model.belongsToMany(models.Series, {
            through: "AuthorSeries",
            as: "series",
            foreignKey: "comicId",
        });
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

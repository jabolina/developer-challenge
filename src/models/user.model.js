const config = require("dotenv").load().parsed;
const bcrypt = require("bcrypt");
const bcryptP = require("bcrypt-promise");
const jwt = require("jsonwebtoken");
const {
    Throws,
    to,
} = require("../services/util.service");

module.exports = (sequelize, DataTypes) => {
    const Model = sequelize.define("User", {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true,
            autoIncrement: true,
        },
        name: DataTypes.STRING,
        email: {
            type: DataTypes.STRING(128),
            allowNull: true,
            unique: true,
            validate: {
                isEmail: {
                    msg: "Not a valid e-mail.",
                },
            },
        },
        phone: {
            type: DataTypes.STRING(128),
            allowNull: true,
            unique: true,
            validate: {
                len: {
                    args: [7, 20],
                    msg: "Phone number invalid, too short.",
                },
                isNumeric: {
                    msg: "Not a valid phone number.",
                },
            },
        },
        password: DataTypes.STRING,
        role: {
            type: DataTypes.STRING,
            allowNull: false,
            enum: ["regular", "editor", "admin"],
            default: "regular",
        },
    });

    Model.beforeSave(async (user) => {
        let err;

        if (user.changed("password")) {
            let salt; let
                hash;

            [err, salt] = await to(bcrypt.genSalt(10));
            if (err) {
                Throws(err.message);
            }

            [err, hash] = await to(bcrypt.hash(user.password, salt));
            if (err) {
                Throws(err.message);
            }

            user.password = hash; // eslint-disable-line no-param-reassign
        }
    });

    Model.prototype.comparePassword = async function comparePassword(hash) {
        if (!this.password) {
            Throws(`Password not set for ${JSON.stringify(this, null, 2)}`);
        }

        const [err, pass] = await to(bcryptP.compare(hash, this.password));
        if (err) {
            Throws(err);
        }

        if (!pass) {
            Throws("Invalid password!");
        }

        return this;
    };

    Model.prototype.getJWT = function getJWT() {
        const sign = jwt.sign({
            user_id: this.id,
        }, config.JWT_ENCRYPTION, {
            expiresIn: parseInt(config.JWT_EXPIRATION, 10),
        });

        return `Bearer ${sign}`;
    };

    Model.prototype.toWeb = function toWeb() {
        return this.toJSON();
    };

    return Model;
};

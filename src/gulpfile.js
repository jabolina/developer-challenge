const gulp = require("gulp");
const concat = require("gulp-concat");
const path = require("path");

const files = {
    root: __dirname,
    srcDir: path.resolve(__dirname, "src"),
    srcFrontDir: path.resolve(__dirname, "views"),
    distDir: path.resolve(__dirname, "dist"),
    frontDistDir: path.resolve(__dirname, "dist/views"),
    ejs: {
        src: path.resolve(path.resolve(__dirname, "views"), "pages"),
        dist: path.resolve(path.resolve(__dirname, "dist/views"), "pages")
    },
    js: {
        src: path.resolve(path.resolve(__dirname, "views"), "js"),
        dist: path.resolve(path.resolve(__dirname, "dist/views"), "js")
    }
};
const ejsSrc = files.ejs.src + "/*.ejs";
const jsSrc = files.js.src + "/*.js";

gulp.task("buildEJS", function (done) {
    gulp.src([ejsSrc])
        .pipe(gulp.dest(files.ejs.dist));

    console.log("EJS finished");
    done();
});

gulp.task("buildJS", function (done) {
    gulp.src([jsSrc])
        .pipe(concat("bundle.min.js"))
        .pipe(gulp.dest(files.js.dist));

    console.log("JS finished");
    done();
});

gulp.task("build:all", gulp.parallel("buildEJS", "buildJS"));
gulp.task("watch", () => {
    console.log("Watching JS: " + jsSrc);
    console.log("Watching EJS: " + ejsSrc);

    gulp.watch(ejsSrc, gulp.parallel("buildEJS"));
    gulp.watch(jsSrc, gulp.parallel("buildJS"));
});

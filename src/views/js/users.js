function registerUser() {
    const userInfo = {
        name: $("#fullNameRegister")[0].value,
        email: $("#emailRegister")[0].value,
        password: $("#passwordRegister")[0].value,
    };

    request(`${window.location.origin}/user/`, "POST", userInfo, false, true, function (response) {
        if (response.success) {
            $("#fullNameRegister").val("");
            $("#emailRegister").val("");
            $("#passwordRegister").val("");
            $("#register-back-button").click();
            $("#loginModal").modal("hide");
            toastr.success(`Cadastro realizado com sucesso, ${response.user.name}`);
        } else {
            toastr.error(response.error);
        }
    });
}

function listUsers() {
    request(`${window.location.origin}/user/`, "GET", {}, true, true, (response) => {
        if (response.success) {
            let i=1;
            let tableContent = "";
            console.log(response);

            response.users.forEach(user => {
                tableContent += `<tr><th scope="row">${i++}</th>`;
                tableContent += `<td>${user.name}</td>`;
                tableContent += `<td>${user.email}</td>`;
                tableContent += `<td>${user.role}</td>`;
                tableContent += `<td><button type='button' onclick='findOne(${user.id})' data-toggle='modal' data-target='#user-info-modal' class='btn btn-outline-primary'>Editar</button></td>`;
                tableContent += `<td><button type="button" onclick="deleteUser(${user.id})" class="btn btn-outline-danger">Excluir</button></td></tr>`;
            });

            $("#user-table-content")[0].innerHTML = tableContent;
        }
    });
}

function deleteUser(id) {
    request(`${window.location.origin}/user/${id}`, "DELETE", {}, true, true, (response) => {
        if (response.success) {
            toastr.success("Usuário excluído com sucesso!");

            setTimeout(() => {
                window.location.reload();
            }, 500);
        } else {
            toastr.error(response.error);
        }
    });
}

function findOne(id) {
    request(`${window.location.origin}/user/${id}`, "GET", {}, true, true, (response) => {
        if (response.success && response.user) {
            const user = response.user;

            $("#user-id-info").val(user.id);
            $("#user-name-info").val(user.name);
            $("#user-email-info").val(user.email);
            $("#user-role-form").val(user.role);
        } else {
            toastr.error(response.error);
        }
    });
}

function updateUser() {
    const body = {
        id: $("#user-id-info").val(),
        email: $("#user-email-info").val(),
        name: $("#user-name-info").val(),
        role: $("#user-role-form").val(),
    };

    request(`${window.location.origin}/user/`, "PUT", body, true, true, function (response) {
        if (response.success) {
            toastr.success(`Usuario atualizado com sucesso!`);

            setTimeout(() => {
                window.location.reload();
            }, 500);
        } else {
            toastr.error(response.error);
        }
    });
}

function logoutUser() {
    window.sessionStorage.clear();
    window.location.href = window.location.origin;
}

function loginUser() {
    const userInfo = {
        email: $("#emailLogin")[0].value,
        password: $("#passLogin")[0].value,
    };

    $("#form-login-user").hide();
    $(".spinner").fadeIn(150);

    request(`${window.location.origin}/user/login`, "POST", userInfo, false, true, function (response) {
        if (response.success) {
            window.sessionStorage.setItem("authorization", response.token);
            window.sessionStorage.setItem("status", (response.user.role === "admin").toString());

            $("#loginModal").modal("hide");
            $("#nav-login-item").css("display", "none");
            toastr.success(`Bem vindo, ${response.user.name}`);

            setTimeout(() => {
                window.location.reload();
            }, 500);
        } else {
            toastr.error(response.error);
        }

        $(".spinner").fadeOut();
        $("#form-login-user").fadeIn();
    });
}

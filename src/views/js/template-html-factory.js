function cardTemplate(data) {
    const cardNode = document.createElement("div");
    let templateHTML =
        `<img class="card-img-top" height="286" width="242" src="${data.thumbnail ? data.thumbnail : data.thumbnailBlob ? data.thumbnailBlob : 'https://placehold.it/286x180'}">`;
    templateHTML += "<div class='card-body'>";
    templateHTML += `<h5 class="card-title">${data.name || data.title || " "}</h5>`;
    templateHTML += `<a href='#' onclick='getInfo("${data.type}", ${data.marvelId}, ${data.id})' class='btn btn-outline-primary' data-toggle='modal' data-target='#info-modal'>Mais informações</a>`;
    templateHTML += "</div>";

    cardNode.setAttribute("class", "card text-center border-secondary mb-3");
    cardNode.style.display = "none";
    cardNode.style.width = "18rem";
    cardNode.innerHTML = templateHTML;

    return cardNode;
}

function wrapCol3(dataArray) {
    const cards = [];

    dataArray.forEach(data => {
        const colNode = document.createElement("div");
        colNode.setAttribute("class", "col-md-3");
        colNode.appendChild(cardTemplate(data));

        cards.push(colNode);
    });

    return cards;
}

function template(mainNode, keepContent, dataArray) {
    const view = document.getElementById(mainNode);

    if (!keepContent) {
        view.innerHTML = "";
    }

    const cols = wrapCol3(dataArray);
    const rows = [];

    let rowNode;

    for (let i=0; i<cols.length; i++) {
        if (i % 4 === 0) {
            rows.push(rowNode);
            rowNode = document.createElement("div");
            rowNode.setAttribute("class", "row cards-margin");
        }

        if (rowNode) {
            rowNode.appendChild(cols[i]);
        }
    }

    rows.push(rowNode);

    for (let i=0; i<rows.length; i++) {
        if (rows[i] && view) {
            view.appendChild(rows[i]);
        }
    }

    $(".spinner").hide();

    $(".card").each(function(index) {
        $(this).delay(250 * index).fadeIn();
    });
}

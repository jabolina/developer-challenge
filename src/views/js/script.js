let enableInfiteScroll = true;

$(() => {
    $(window).on("hashchange", hashChanged);
});

function hashChanged() {
    const hash = location.hash.replace(/[#\/]/g, "") || "main";
    $("#application-main-content").load(`./views/${hash}.ejs`);
}

function loadLastPage() {
    hashChanged();
}

function getInfo(type, marvelId, id) {
    switch (type) {
        case "character":
            getCharacterById(marvelId, id);
            break;
        case "author":
            getAuthorById(marvelId, id);
            break;
        case "series":
            getSeriesById(marvelId, id);
            break;
        default: break;
    }
}

function hideNonAuthItems () {
    if (!isAuthenticated()) {
        $(".insert-new").css("display", "none");
    } else {
        if (window.sessionStorage.getItem("status") === "true") {
            $("#user-nav-item").css("display", "inline");
        }

        $("#nav-login-item").css("display", "none");
        $("#nav-logout-item").css("display", "block");
    }
}

function isAuthenticated() {
    return !!window.sessionStorage.getItem("authorization");
}

$(document).ready(() => {
    loadLastPage();
});

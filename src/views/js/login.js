function hideAll() {
    $("#choose-login-register").hide();
    $(".user_register").hide();
    $(".user_login").hide();
}

$(() => {
    $("#login-modal-btn").on("click", () => {
        hideAll();
        $(".user_login").show();
        return false;
    });

    $("#register-modal-btn").on("click", () => {
        hideAll();
        $(".user_register").show();
        return false;
    });

    $(".back_btn").on("click", () => {
        $(".user_login").hide();
        $(".user_register").hide();
        $("#choose-login-register").show();
        return false;
    });
});

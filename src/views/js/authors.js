function getAuthors(offset, keepContent) {
    enableInfiteScroll = true;

    request(`${window.location.origin}/author?offset=${offset}&limit=12`, "GET", {}, false, true, (response) => {
        if (response.success && response.authors.length > 0) {
            template("authors-main-content", keepContent, response.authors);
        }
    });
}

function searchAuthor() {
    const authorName = $("#author-search")[0].value;

    if (authorName) {
        enableInfiteScroll = false;
        document.getElementById("authors-main-content").innerHTML = "";
        $(".spinner").fadeIn();

        request(`${window.location.origin}/author/name?nameStartsWith=${authorName}`, "GET", {}, false, true, (response) => {
            if (response.success) {
                template("authors-main-content", false, response.authors);
            }
        });
    } else {
        getAuthors(0, false);
    }
}

function createAuthor() {
    const file = $("#author-thumbnail-form");
    const name = $("#author-name-form")[0].value;
    if (!file[0].files[0] || !name) {
        toastr.warning("Informe todos os campos");
    } else {
        fileToB64(file[0].files[0], function (base64) {
            const authorInfo = {
                name,
                description: "",
                thumbnail: base64,
                modified: new Date(),
            };

            request(`${window.location.origin}/author/`, "POST", authorInfo, true, true, function (response) {
                if (response.success) {
                    toastr.success("Personagem cadastrado com sucesso!");
                    $("#reset-author-form")[0].click();
                    $("#register-author-modal").modal("hide");
                } else {
                    toastr.error(`Error no cadastro do autor: ${response.error}`);
                }
            });
        });
    }
}

function getAuthorById(marvelId, id) {
    if (!isAuthenticated() || true) {
        $("#update-author-modal-btn").css("display", "none");
    }

    request(`${window.location.origin}/author/id/${marvelId || id}?db=${!marvelId}`, "GET", {}, false, true, (response) => {
        if (response.success) {
            const author = response.author;

            $("#author-info-thumbnail").attr("src", author.thumbnail);
            $("#author-name-info").val(author.name);

            let comics = "";
            if (author.comics) {
                author.comics.forEach(comic => {
                    comics += `${comic.name}, `;
                });
            }
            comics = comics.slice(0, -2);
            $("#author-comic-info").val(comics);

            let series = "";
            if (author.series) {
                author.series.forEach(serie => {
                    series += `${serie.title}, `;
                });
            }
            series = series.slice(0, -2);
            $("#author-series-info").val(series);

            let story = "";
            if (author.stories) {
                author.stories.forEach(s => {
                    story += `${s.name}, `;
                });
            }
            story = story.slice(0, -2);
            $("#author-story-info").val(story);

            let event = "";
            if (author.events) {
                author.events.forEach(e => {
                    event += `${e.name}, `;
                });
            }
            event = event.slice(0, -2);
            $("#author-event-info").val(event);

            $("#info-data").css("display", "block");
        } else {
            toastr.error(`Erro buscando mais informações ${response.error}`);
        }
    });
}

$(document).ready(() => {
    (() => {
        const win = $(window);
        const doc = $(document);

        let i=1;

        win.scroll(() => {
            if (win.scrollTop() + win.height() > doc.height() - 40 && enableInfiteScroll) {
                $(".spinner").fadeIn();
                getAuthors((i++)*12, true);
            }
        });
    })();
});

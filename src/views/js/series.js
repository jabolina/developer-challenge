function getSeries(offset, keepContent) {
    enableInfiteScroll = true;

    request(`${window.location.origin}/series?offset=${offset}&limit=12`, "GET", {}, false, true, (response) => {
        if (response.success && response.series.length > 0) {
            template("series-main-content", keepContent, response.series);
        }
    });
}

function searchSeries() {
    const seriesName = $("#series-search")[0].value;

    if (seriesName) {
        enableInfiteScroll = false;
        document.getElementById("series-main-content").innerHTML = "";
        $(".spinner").fadeIn();

        request(`${window.location.origin}/series/name?titleStartsWith=${seriesName}`, "GET", {}, false, true, (response) => {
            if (response.success) {
                template("series-main-content", false, response.series);
            }
        });
    } else {
        getSeries(0, false);
    }
}

function createSeries() {
    const file = $("#series-thumbnail-form")[0].files[0];
    const title = $("#series-name-form")[0].value;
    const startYear = $("#series-creation-form")[0].value;

    if (!file || !title || !startYear) {
        toastr.warning("Informe todos os campos");
    } else {
        fileToB64(file, function (base64) {
            const seriesInfo = {
                title,
                startYear,
                thumbnail: base64,
                modified: new Date(),
            };

            request(`${window.location.origin}/series/`, "POST", seriesInfo, true, true, function (response) {
                if (response.success) {
                    toastr.success("Série cadastrada com sucesso!");
                    $("#reset-series-form")[0].click();
                    $("#register-series-modal").modal("hide");
                } else {
                    toastr.error(`Error no cadastro da série: ${response.error}`);
                }
            });
        });
    }
}

function getSeriesById(marvelId, id) {
    if (!isAuthenticated() || true) {
        $("#update-series-modal-btn").css("display", "none");
    }

    request(`${window.location.origin}/series/id/${marvelId || id}?db=${!marvelId}`, "GET", {}, false, true, (response) => {
        if (response.success) {
            const series = response.series[0] || response.series;

            $("#series-info-thumbnail").attr("src", series.thumbnail);
            $("#series-name-info").val(series.title);

            let creators = "";
            if (series.creators) {
                series.creators.forEach(creator => {
                    creators += `${creator.name}, `;
                });
            }
            creators = creators.slice(0, -2);
            $("#series-creator-info").val(creators);

            let comics = "";
            if (series.comics) {
                series.comics.forEach(comic => {
                    comics += `${comic.name}, `;
                });
            }
            comics = comics.slice(0, -2);
            $("#series-comic-info").val(comics);

            let story = "";
            if (series.stories) {
                series.stories.forEach(s => {
                    story += `${s.name}, `;
                });
            }
            story = story.slice(0, -2);
            $("#series-story-info").val(story);

            let event = "";
            if (series.events) {
                series.events.forEach(e => {
                    event += `${e.name}, `;
                });
            }
            event = event.slice(0, -2);
            $("#series-event-info").val(event);

            const now = new Date();
            const day = ("0" + now.getDate()).slice(-2);
            const month = ("0" + (now.getMonth() + 1)).slice(-2);

            const todayYearsAgo = `${series.startYear}-${month}-${day}`;
            $("#series-year-info").val(todayYearsAgo);

            $("#info-data").css("display", "block");
        } else {
            toastr.error(`Erro buscando mais informações ${response.error}`);
        }
    });
}

$(document).ready(() => {
    hideNonAuthItems();

    (() => {
        const win = $(window);
        const doc = $(document);

        let i=1;

        win.scroll(() => {
            if (win.scrollTop() + win.height() > doc.height() - 40 && enableInfiteScroll) {
                $(".spinner").fadeIn();
                getSeries((i++)*12, true);
            }
        });
    })();
});

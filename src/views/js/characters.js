function getCharacters(offset, keepContent) {
    enableInfiteScroll = true;

    request(`${window.location.origin}/marvel?offset=${offset}&limit=12`, "GET", {}, false, true, (response) => {
        if (response.success) {
            template("characters-main-content", keepContent, response.characters);
        }
    });
}

function searchCharacter() {
    const characterName = $("#character-search")[0].value;

    if (characterName) {
        enableInfiteScroll = false;
        document.getElementById("characters-main-content").innerHTML = "";
        $(".spinner").fadeIn();

        request(`${window.location.origin}/marvel/name?nameStartsWith=${characterName}`, "GET", {}, false, true, (response) => {
            if (response.success) {
                template("characters-main-content", false, response.characters);
            }
        });
    } else {
        getCharacters();
    }
}

function createCharacter() {
    const file = $("#character-thumbnail-form")[0].files[0];
    const name = $("#character-name-form")[0].value;
    const description = $("#character-description-form")[0].value;

    if (!file || !name) {
        toastr.warning("Informe todos os campos");
    } else {
        fileToB64(file, function (base64) {
            const characterInfo = {
                name,
                description,
                thumbnail: base64,
                modified: new Date(),
            };

            request(`${window.location.origin}/marvel/`, "POST", characterInfo, true, true, function (response) {
                if (response.success) {
                    toastr.success("Personagem cadastrado com sucesso!");
                    $("#reset-character-form")[0].click();
                    $("#register-character-modal").modal("hide");
                } else {
                    toastr.error(`Error no cadastro do personagem: ${response.error}`);
                }
            });
        });
    }
}

function getCharacterById(marvelId, id) {
    if (!isAuthenticated() || true) {
        $("#update-character-modal-btn").css("display", "none");
    }

    request(`${window.location.origin}/marvel/id/${marvelId || id}?db=${!marvelId}`, "GET", {}, false, true, (response) => {
        if (response.success) {
            const character = response.character;

            $("#character-info-thumbnail").attr("src", character.thumbnail);
            $("#character-name-info").val(character.name);
            $("#character-description-info").val(character.description);

            let comics = "";
            if (character.comics) {
                character.comics.forEach(comic => {
                    comics += `${comic.name}, `;
                });
            }
            comics = comics.slice(0, -2);
            $("#character-comic-info").val(comics);

            let series = "";
            if (character.series) {
                character.series.forEach(serie => {
                    series += `${serie.title}, `;
                });
            }
            series = series.slice(0, -2);
            $("#character-series-info").val(series);

            let story = "";
            if (character.stories) {
                character.stories.forEach(s => {
                    story += `${s.name}, `;
                });
            }
            story = story.slice(0, -2);
            $("#character-story-info").val(story);

            let event = "";
            if (character.events) {
                character.events.forEach(e => {
                    event += `${e.name}, `;
                });
            }
            event = event.slice(0, -2);
            $("#character-event-info").val(event);

            $("#info-data").css("display", "block");
        } else {
            toastr.error(`Erro buscando mais informações ${response.error}`);
        }
    });
}

$(document).ready(() => {
    (() => {
        const win = $(window);
        const doc = $(document);

        let i=1;

        win.scroll(() => {
            if (win.scrollTop() + win.height() > doc.height() - 40 && enableInfiteScroll) {
                $(".spinner").fadeIn();
                getCharacters((i++)*12, true);
            }
        });
    })();
});

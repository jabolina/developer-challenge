function request(url, method, body, auth, async, cb) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url, async);
    xhr.setRequestHeader("Content-Type", "application/json");

    if (auth) {
        xhr.setRequestHeader("Authorization", window.sessionStorage.getItem("authorization"));
    }

    xhr.onload = function () {
        if (this.readyState === 4) {
            try {
                cb(JSON.parse(this.responseText));
            } catch (e) {
                throw e;
            }
        }
    };

    xhr.send(JSON.stringify(body || {}));
}
